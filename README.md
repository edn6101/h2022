# EDN6101 Édition critique avec la TEI

**[Lien vers le site de l'atelier](https://edn6101.gitpages.huma-num.fr/h2022)**

## Les outils pour travailler à distance

- **ressources** (supports de présentation, exercices) : [ce dépôt](https://gitlab.huma-num.fr/edn6101/h2022)
- **messages longs** : [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca) ou [emmanuel.chateau.dutier@umontreal.ca](emmanuel.chateau.dutier@umontreal.ca)

## Fonctionnement du site ressource
Le site est généré avec [Hugo](https://gohugo.io/) :

- installer Hugo : [https://gohugo.io/getting-started/installing/](https://gohugo.io/getting-started/installing/)
- faire tourner Hugo en local : `hugo serve`

Les supports de présentation peuvent être visualisés en local :

- aller dans le dossier `presentations`, par exemple avec `cd presentations`
- lancer un serveur et visualiser dans le navigateur, par exemple avec Python : `python3 -m http.server` puis [localhost:8000](http://localhost:8000/)
