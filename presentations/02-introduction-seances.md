## EDN6101 - Édition critique avec la TEI

# Programme de la formation
Antoine Fauchié, février 2022

Site web pour les ressources du cours :  
[https://edn6101.gitpages.huma-num.fr/h2022](https://edn6101.gitpages.huma-num.fr/h2022)

???

Bonjour,

Je suis heureux de vous accueillir à cet atelier d’initiation à l’édition critique avec la TEI.

- premier atelier d’introduction à XML
- Elle sera par la suite complétée par une autre formation courte d’introduction à la Text Encoding Initiative.

Je m'appelle Antoine Fauchié, je suis doctorant au Département de littératures et de langues du monde, et coordinateur des projets de la Chaire de recherche du Canada sur les écritures numériques. Je travaille sur les processus de publication numérique.

Pour la deuxième séance Emmanuel Château-Dutier nous rejoindra, il est professeur-adjoint en muséologie numérique à l’Université de Montréal. Il est engagé depuis plusieurs années sur divers projets d’édition structurées. Il assure notamment avec Lou Burnard à l’École nationale des chartes à Paris la formation continue de l’École des chartes sur la Text Encoding Initiative qui offre un cadre de travail pour l’édition électronique et qui se trouve notamment exprimée en XML.

---

## Tour de table

* Qui êtes-vous&nbsp;?
* Sur quoi portent vos recherches&nbsp;?
* Ce qui vous intéresse avec cette formation.

---

## Objectifs de l'atelier

- Connaître les grandes lignes du langage
- Se familiariser avec la syntaxe
- Identifier les applications

???

## Contenu de la formation

Cette formation est une initiation à XML. Après un rapide présentation du langage et de ses applications, les participants se familiariseront avec la syntaxe XML par l’intermédiaire de travaux pratiques.

Les thématiques abordées au cours de la formation sont les suivantes :

* Historique et application de XML
* Principes de conception du langage
* La syntaxe XML
* Notions de document bien formé et de document valide
* Les technologies de schémas (modéliser des sources avec XML)
* Langages associés et exploitation des documents

---

## Approche pédagogique

- Aucun pré-requis informatique
- Plusieurs travaux pratiques
- Une perspective humaniste

???

## Approche pédagogique

- Aucun pré-requis informatique : On part du principe que vous êtes novices en informatique.
- Une démarche progressive, adossée à des Travaux pratiques.
- Une perspective humaniste

---

## Séance 1 - samedi 5 février 2022 9h30-16h30

- présentation des intervenants et des participant·e·s ;
- le langage de balisage XML et son écosystème ;
- exercices pratiques d'encodage en XML ;
- présentation de la Text Encoding Initiative (TEI) ;
- l’utilisation de la TEI pour l’édition critique ;
- exercice de structuration de texte avec la TEI.

---

## Séance 2 - samedi 19 février 2022 9h30-16h30

- utilisation la TEI pour l’édition de sources manuscrites ;
- modélisation d’une édition critique avec la TEI ;
- exercices de mise en pratique de structuration et de modèles ;
- méthodes de travail avec la TEI ;
- modalités de transformation avec XSLT ;
- exercices d’application de transformation ;
- bilan des 4 séances.

---

???
