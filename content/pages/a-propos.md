---
title: À propos
date: 2022-01-20
---
Bienvenue sur le site web de l'atelier EDN6101 Édition critique avec la TEI.

Ce cours regroupe les contenus du site, il est destiné aux étudiant·e·s qui suivent l'atelier à l'hiver 2022.

Contact : Antoine Fauchié ([antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)) et Emmanuel Château-Dutier ([emmanuel.chateau.dutier@umontreal.ca](mailto:emmanuel.chateau.dutier@umontreal.ca))
