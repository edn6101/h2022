---
title: Plan d'atelier
toc: oui
impression: oui
date: 2022-01-26
bibfile: "data/bibliographie.json"
description: "L'atelier EDN6101 Édition critique avec la TEI est un cours offert dans le cadre du programme d'études DESS en édition numérique."
---
Contacts : Antoine Fauchié ([antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)), et Emmanuel Château-Dutier ([emmanuel.chateau.dutier@umontreal.ca](emmanuel.chateau.dutier@umontreal.ca)).

## Objectifs de l'atelier
Cet atelier vise à initier les étudiantes et les étudiants aux méthodes d’édition critique avec la Text Encoding Encoding Initiative (TEI), un cadre de travail pour la production d’édition structurée fondé sur le langage de balisage XML (eXtended Markup Language).

Au terme du cours, l’étudiant·e sera en mesure :

- d’expliquer les principes de l’édition structurée et la notion de balisage descriptif ;
- de réaliser un encodage en XML-TEI ;
- d’effectuer certaines personnalisations possibles de la TEI ;
- d’utiliser les langages XPath et XSL pour mettre en ligne une édition.

## Calendrier des séances
L'atelier se déroule les samedis 5 et 19 février 2022 de 9h30 à 16h30 dans le local C-2043 du Pavillon Lionel Groulx.

### Séance 1 - samedi 5 février 2022 9h30-16h30

- présentation des intervenants et des participant·e·s ;
- le langage de balisage XML et son écosystème ;
- exercices pratiques d'encodage en XML ;
- présentation de la Text Encoding Initiative (TEI) ;
- l’utilisation de la TEI pour l’édition critique ;
- exercice de structuration de texte avec la TEI.

### Séance 2 - samedi 19 février 2022 9h30-16h30

- utilisation de la TEI pour l’édition de sources manuscrites ;
- modélisation d’une édition critique avec la TEI ;
- exercices de mise en pratique de structuration et de modèles ;
- méthodes de travail avec la TEI ;
- modalités de transformation avec XSLT ;
- exercices d’application de transformation ;
- bilan des 4 séances.

## Évaluation
L’évaluation de l’atelier reposera sur des travaux pratiques d’encodage en XML/TEI :

1. Exercice pratique d'encodage en XML (30%)
1. Édition critique de sources manuscrites (40%)
2. Personnalisation d’un schéma TEI (40%)

## Bibliographie

{{< bibliography >}}
