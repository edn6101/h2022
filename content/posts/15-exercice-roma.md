+++
author = "Antoine"
title = "15 - Exercice : personnaliser la TEI avec Roma"
date = "2022-02-15"
description = "Exercice : personnaliser la TEI avec Roma"
seance2 = "oui"
impression = "oui"
published = true
+++
L’objet de cet exercice consiste à créer une personnalisation de la TEI (un fichier ODD) pour l’encodage du manuscrit sur lequel nous avons travaillé jusqu’à présent.

Pour ce faire, nous allons utiliser l’outil [Roma](https://roma2.tei-c.org/) mis à disposition par la Text Encoding Initiative. Celui-ci propose une interface graphique qui facilite la création d’une personnalisation de la TEI. 

Le logiciel que nous allons utiliser est actuellement en version bêta, il remplace une version plus ancienne accessible à [cette adresse](https://roma2.tei-c.org/).

## Objectif de l’exercice

- Cibler des modules de la TEI pour créer un schéma personnalisé
- Sélectionner un sous-ensemble d’éléments des modules TEI
- Associer un schéma personnalisé avec un document TEI-XML dans oXygen
- Modifier les valeurs autorisées des attributs disponibles dans un schéma
- Générer automatiquement la documentation d’un schéma avec Roma
- Se familiariser avec le format TEI ODD XML sous-jacent

## Pour celles et ceux qui ne seraient pas suffisamment avancé·e·s
**Uniquement pour celles et ceux qui ne se sentiraient pas suffisamment à l'aise avec leur fichier comme base de travail à cet exercice**, vous pouvez utiliser [ce fichier XML](/corriges/acteRoyalCorr1.tei.xml).

## Roma

**[https://romabeta.tei-c.org/](https://romabeta.tei-c.org/)**

Roma est une application en ligne qui offre une interface graphique qui permet de sélectionner les modules de la TEI, des éléments ou des attributs afin de personnaliser un schéma.

Plusieurs approches sont possibles :

- _Build up_ : ajouter ce qu’il vous faut à un schéma minimal
- _Reduce_ : prenez tout ce qui existe et enlever ce qui ne vous intéresse pas
- _Upload_ : création d’un schéma à partir d’un modèle ou retravailler une personnalisation déjà existante

Nous allons procéder en étendant un schéma existant. Dans la liste des ODD proposés, choisissez **TEI Minimal (customize by building TEI up)** puis appuyez sur `start`.

## Réglage des paramètres

- _Title_ : Changez _TEI absolutely bare_ en _TEI pour les manuscrits_ (par exemple).
- _Identifier_ : Change _tei_minimal_ en _teiTranscr_ (par exemple) (c’est un identifiant, et donc ne pas comporter d’espace)
- _Customization Namespace_ : ce champ est utile dans le cas où nous voudrions étendre la TEI en créant de nouveaux éléments. Ces éléments seraient placés dans un nouvel espace de nom.
- _Prefix_ : conserver `tei_`
- _Language of elements and attributes_ : cela vous permet de sélectionner la langue des éléments TEI. Conserver l’anglais.
- _Documentation Language_ : On choisit le français pour la documation (définition des éléments, etc.).
- _Author_ : Entrez votre nom

Cliquez sur le bouton  `customize ODD` en haut à droite de l’écran.

## Sélection des modèles TEI

Afin de personnaliser le modèle, nous vous proposons de procéder en commençant par cibler les modules de la TEI dont nous avons besoin. Un module est un regroupement d’éléments dans la Text et tous les éléments de la TEI sont déclarés dans un module spécifique. Ces modules correspondent généralement à des cas d’usages particulier. Par exemple, pour encoder un dictionnaire vous aurez besoin des éléments spécialisés pour les dictionnaires dans le module `dictionnaries`, pour les texte dramatique des éléments définis dans le module `drama`. Mais tous ces éléments ne sont pas forcément intéressants pour l’encodage d’autres types de documents. Or, nous cherchons à spécifier le schéma le plus restrictif possible pour notre cas d’usage, l’encodage d’un manuscrit.

En sélectionnant **TEI Minimal (customize by building TEI up)** au moment de la création du fichier tout à l’heure, nous avons présélectionné un ensemble minimal de balise. 

- Explorez la liste pour repérer les éléments TEI sélectionnés (case à cocher à gauche) ainsi que le module auxquels ils appartiennent (étiquette à droite de la ligne). 
- Vous devriez reconnaître un certain nombre d’éléments que nous avons déjà croisés au cours de cette formation.
- Peut-être allez-vous aussi repérer des noms d’éléments dont nous avons besoin et qui n’ont pas été sélectionnés.

Pour commencer à personnaliser notre schéma, essayons de repérer les modules dont nous avons besoin. Voici la [liste des modules de la TEI](https://tei-c.org/release/doc/tei-p5-doc/en/html/ST.html#STMA).

- Dans Roma, cliquez sur le bouton `by module` et parcourez la listes des éléments les modules ajoutés au schéma sont `core`, `header`, `textstructure`, `tei` (non affiché dans la liste).
- Comme nous souhaitons disposer des balises pour traiter la transcription des manuscrits. Nous aurons besoin donc d’ajouter des éléments se trouvant dans au moins deux modules supplémentaires. **Lesquels ?**
- Ajouter ensuite ces modules en cliquant sur le tag du nom du module qui porte un signe `+`

{{< details "Quels sont les modules supplémentaires nécessaires ?" >}}

{{< /details>}}

## Inclusion et exclusion des éléments (1)

Vous devriez maintenant disposer d’un schéma construit à partir de plusieurs modules de la TEI. Dans cet ensemble restreint de modules, il reste tout de même à sélectionner les éléments dont nous avons besoin pour notre projet. Afin d’obtenir un ensemble consistant de documents, nous avons besoin de restreindre le plus possible les éléments disponibles pour l’éditeur.

- Chaque ligne de cette table contient :
  - le nom canonique d’un élément
  - une description brève des fonctions de cet élément
  - une indication de son Inclusion ou Exclusion (case à cocher)
- Pour chacun des éléments correspondants aux modules sélectionnés, vous pouvez passer en revue les éléments et leur définition. Afin d’en savoir plus sur un élément, cliquer sur la case à cocher puis son nom, le bloc Documentation vous donne accès à la définition de l’élément, `Attribues` aux attributs disponibles sur cet élément, et `Class Membership & Content Model` aux apparatenances de classes déclarées pour cet élément.
- Cette interface vous permet d’explorer la fonction et l’usage de tous les éléments TEI. Elle vous permet également de sélectionner ceux que vous souhaitez intégrer dans votre schéma. Concentrons nous pour le moment seulement sur les définitions. Notez que vous pouvez naviguer dans cette documentation dans la barre de menu.


## Inclusion et exclusion des éléments (2)

* ouvrir `static/corriges/acteRoyalCorr1.tei.xml` dans oXygen
* Étudier le balisage mis en œuvre
* pour chacun des modules que vous avez choisis, sélectionner les éléments nécessaires pour pouvoir valider votre document.


## Génération d’un fichier ODD

Roma vous permet de créer directement un fichier ODD qui spécifie votre personnalisation de la TEI et un nouveau schéma à partir des choix que vous venez de déclarer.

Dans le menu `Download`, vous pouvez enregistrer le fichier ODD avec `Customization as ODD` ou choisir le type de schéma à télécharger. Le fichier ODD est le plus précieux car c’est celui qui contient toute votre personnalisation. Mais pour travailler avec l’éditeur XML on utilise un schema XML. En général, nous vous conseillons de générer un schéma en Relax NG Compact Syntax, ou en Relax NG XML Syntax.

- Cliquer sur `Download` puis sélectionner `Customization as ODD` pour enregistrer votre personnalisation sur votre ordinateur.
- Ouvrer ce fichier avec oXygen (dans oXygen `Fichier>ouvrir avec`), et observer son contenu. C’est un fichier TEI qui contient des déclarations `<schemaSpec>` avec des références aux modules et aux éléments que vous avez sélectionnés dans l’interface. Vous retrouvez également les informations que vous avez saisies dans l’entête du document.
- Roma vous permet d’écrire un fichier de personnalisation assez facilement. Noter qu’avec un peu de connaissance du langage ODD, vous aurriez tout aussi bien pu écrire ce fichier manuellement !

## Production d’un schéma Relax-NG

Deux solutions s’offrent à vous pour générer un schéma Relax-NG à partir de votre personnalisation. 

1° Vous pouvez générer le schéma directement depuis Roma

- Cliquez sur `Download` puis sélectionner `RelaxNG schema` pour enregistrer votre personnalisation sur votre ordinateur.
- Pour faciliter les manipulations, enregistrer ce fichier dans le même répertoire que celui où vous avez téléchargé le fichier TEI à valider.

2° Vous pouvez également générer un schema RelaxNG depuis Oxygen à partir du fichier ODD que vous avez téléchargé à l’étape précédente.

- Ouvrir le fichier ODD dans oXygen
- Dans le menu _Document>Transformation>appliquez les scénario de transformation_, choisissez TEI ODD to RelaxNG XML. (attention à bien choisir XML). Si ce choix n’apparaît pas, appuyer sur l’engrenage et choississez `afficher tout` puis cherchez ODD dans la liste.
- Pour faciliter les manipulations, enregistrer ce fichier dans le même répertoire que celui où vous avez téléchargé le fichier TEI à valider.


## Association du schéma personnalisé

- dans oXygen et ouvrez votre fichier encodé.
- Dans le menu _Document_ sélectionner le menu _Schéma_ et sélectionnez _Associer un schéma_. (Notez l’icône bleue et rouge pour cette action qui devrait également apparaître sur la barre à outils)
- Dans la boîte de dialogue qui s’affiche, cliquez sur la petite icône de dossier à côté de URL pour naviguer vers le fichier RNG que vous venez de créer avec Roma. Cliquez sur OK.
- Dans votre fichier XML-TEI, vous devrez maintenant voir une ligne qui ressemble à ceci&nbsp;:

 ```xml
<?xml-model href="teiTranscr.rng"
  type="application/relaxng-compact-syntax"
  ns="http://relaxng.org/ns/structure/1.0"?>
 ```
S’il y a d’autres lignes du même type, enlevez-les !

- Vous pouvez maintenant valider contre le schéma votre document. Il est très probable que vous rencontriez des erreurs parceque vous aurez oublié de déclarer certains éléments. C’est normal ! 
- Prener connaissance des messages d’erreur et retourner dans Roma pour modifier votre personnalisation. Si jamais vous avez fermé votre navigateur, vous pouvez repartir de votre personnalisation en chargeant sur la page d’accueil le fichier ODD.
- **Essayez de répéter l’opération jusqu’à ce que vous obteniez un document valide.**


## Utilisation du schéma personnalisé

### Comment limiter les valeurs de l’attribut @type de l’élément `<div>`&nbsp;?

Nous avons vu qu’il est très utile de supprimer tel ou tel élément de notre schéma. Que peut-on faire pour limiter les valeurs légales de ses attributs ?

(Notez qu’en général les attributs TEI sont définis d’une manière très permissive : vous pouvez taper n’importe quelle valeur pour un attribut). Supposons que nous souhaitions limiter les valeurs possibles pour l’attribut @type sur `<div>`.

- Dans Roma, repérer le rang ou est défini l’élément `<div>` et cliquez sur son nom.
- Vous avez alors accès à la personnalisation des attributs et aux apparatenances de classes et aux modèles de contenu.
- Dans le bloc `Attribute` chez l’attribut _@type_ et cliquer sur le bouton modifier en vis-à-vis du nom de l’attribut. Une page s’affiche, qui vous permet de définir plusieurs options pour cet attribut&nbsp;:

  - _Usage_ vous permet de définir si l’attribut est obligatoire ou optionnel. Supposons que nous voulions que la présence de l’attribut soit obligatoire, dans ce cas sélectionner `Required`.
  - _Description_ vous permet de modifier la description de l’élément. En général on conserve la définition de la TEI, mais vous pourriez vouloir préciser l’usage de cet élément.
  - _Values_ vous permet de définir une liste fermée ou ouverte de valeurs pour cette attribut. Par exemple vous pourriez conserver une liste semi-ouverte et proposer à votre éditeur une liste pré-définie de valeurs. Créer les en tappant du texte dans le champ puis en cliquant sur `+`. Le champ description vous permet de proposer à l’éditeur une définition pour chaque valeur.

Vous pouvez maintenant générer ce nouveau schéma puis tester son utilisation. Observer l’effet de vos modifications.

Sachez qu’il est également possible avec Roma de travailler sur les modèles de contenus et les classes d’attribut pour des personnalisations plus avancées.


### Génération de documentation

Tout projet aura besoin de sa documentation interne, qui ne sera pas forcément optimale si elle est en RelaxNG ! Roma vous permet de générer automatiquement des spécifications compréhensibles, ressemblant à la doc de référence de la TEI.

- Retourner dans votre navigateur et cliquez sur `Dowload` puis sélectionner `Documentation as HTML`
- Après un bref délai, votre navigateur va recevoir un fichier HTML que vous pouvez lire avec le navigateur. Au début il y a une table des matières, avec un lien pour chaque élément de votre schéma. Scrollez jusqu’au lien pour `<div>` et cliquez dessus.
- Noter que la description de son attribut `@type` est modifiée selon vos propositions. (Mais noter aussi que d’autres détails, notamment les exemples d’usage, n’ont pas été modifiés) .

Comme tout à l’heure pour générer un schéma depuis un fichier ODD, vous pouvez générer cette documentation à partir du fichier ODD avec oXygen.

- Sauver maintenant votr personnalisation comme une `Customization as ODD`.
- Ouvrer ce fichier dans oxygen puis cliquer sur `Scénario de transformation`, dans la liste ODD sélectionner le format voulu.


## Pour poursuivre une spécification

Le fichier de personnalisation ODD sauvegardé en XML peut être réimporté dans Roma, afin d'adapter et d'améliorer un schéma déjà personnalisé.

---

## Pour en savoir plus

Voici quelques lectures intéressantes (mais en anglais) à ce sujet :

### Documentation de référence (dans les Guidelines)

- Description complète du langage ODD : http://www.tei-c.org/release/doc/tei-p5-doc/en/html/TD.html.
- Description du traitement des fichiers ODD : http://www.tei-c.org/release/doc/tei-p5-doc/en/html/USE.html#IM

### Autres documents tutoriels

- Getting started with P5 ODDs http://tbe.kantl.be/TBE/modules/TBED08v00.htm
- http://www.tei-c.org/Guidelines/Customization/odds.xml