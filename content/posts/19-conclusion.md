+++
author = "Antoine"
title = "19 - Conclusion"
date = "2022-02-18"
description = "Conclusion"
bonus = "oui"
seance2 = "oui"
impression = "oui"
published = true
+++
Pour conclure cet atelier :

- les corrigés peuvent être téléchargés depuis [ce fichier compressé](/corriges.zip) ;
- vous faites maintenant partie de la communauté TEI, bravo ! Pour vous abonner à la liste de diffusion francophone, suivez ce lien : [https://groupes.renater.fr/wiki/tei-fr/](https://groupes.renater.fr/wiki/tei-fr/).
