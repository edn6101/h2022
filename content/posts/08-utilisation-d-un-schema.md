+++
author = "Emmanuel"
title = "08 - Exercice : utilisation d'un schéma"
date = "2022-02-02"
description = "Exercice : utilisation d'un schéma"
seance1 = "oui"
+++
Pour organiser votre espace de travail pendant les exercices : vous devez placer tous vos fichiers XML dans un même répertoire, notamment pour que le lien entre les fichiers XML et les schémas soient relatifs.

Voici le schéma à télécharger avec une base de fichier XML : [schéma (faites un clic droit et "enregistrer la cible du lien sous", puis dézipper le dossier zippé dans votre espace de travail)](/tp02.zip)
