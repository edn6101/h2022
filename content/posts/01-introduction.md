+++
author = "Antoine"
title = "01 - À lire et à faire avant la première séance"
date = "2022-01-30"
description = "À lire et à faire avant la première séance du samedi 6 février 2021 (9h-12h30)."
seance1 = "oui"
bibfile = "data/bibliographie.json"
+++
## Lectures
Avant la première séance voici deux textes à lire :

- sur les questions de texte numérique et de balisage sémantique : {{< cite derose_what_1990 >}} ;
- une introduction la TEI : {{< cite burnard_introduction_2015 >}} ;
- références complètes :

{{< bibliography cited >}}

## Logiciel oXygen à installer
Pendant les 4 séances vous aurez besoin d'utiliser le logiciel oXygen en version 23, **merci de l'installer avant la première séance afin de gagner du temps**, voici les détails pour l'installation :

1. rendez-vous sur la page Logithèque de l'Université de Montréal : [https://logitheque.ti.umontreal.ca](https://logitheque.ti.umontreal.ca) ;
2. connectez-vous avec votre compte de l'Université de Montréal ;
3. acceptez les termes de la directive ;
4. sélectionnez le logiciel oXygen 23 pour Linux, Windows ou Mac selon le système d'exploitation de votre ordinateur ;
5. cliquez sur "Installation du logiciel", et téléchargez l'archive zippée ;
6. décompresser le fichier téléchargé, selon votre système d'exploitation vous devrez exécuter un fichier (l'autre fichier contient la clé permettant d'activer la licence) ;
7. une fois l'installation lancée, vous devrez copier-coller la clé contenue dans le fichier texte (vous pouvez utiliser un éditeur de texte pour ouvrir ce fichier dont l'extension est `.txt`).

En cas de problème [écrivez-moi](mailto:antoine.fauchie@umontreal.ca) en me décrivant le problème que vous rencontrez.
