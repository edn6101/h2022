+++
author = "Antoine"
title = "05 - Exercice : modéliser un carnet d'adresse"
date = "2022-02-02"
description = "Exercice : carnet d'adresse"
seance1 = "oui"
+++
# Modéliser un carnet d’adresse

XML est un méta-langage extensible qui peut être utilisé pour représenter toutes sortes de données. **En tirant parti de la syntaxe XML représentez un carnet d’adresse en essayant d’encoder des exemples.** Vous êtes libres de choisir les noms d’éléments et d’attributs de même que la structuration la plus appropriée pour prendre en charge l’information.

Votre carnet d’adresse comporte les informations suivantes&nbsp;:

* prénom et nom
* adresse postale
* courriel
* téléphone professionnel
* téléphone personnel
* etc. (et toutes les informations que vous jugerez utiles)
